# -*- coding: utf-8 -*-
import argparse
import logging
import os

import numpy as np

from chromo import Chromosome, split_random

logging.basicConfig(level=logging.INFO)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-a', '--atom', default=4, type=int, help='bp kinds')
    parser.add_argument('-n', '--number', default=30, type=int, help='number of chromosome in a group')
    parser.add_argument('-g', '--group', default=6, type=int, help='number of groups')
    parser.add_argument('-l', '--length', default=100000000, type=int, help='average length of a chromosome')
    parser.add_argument('-s', '--std', default=100000, type=float, help='std dev of chromosome length')
    parser.add_argument('--max-cut', default=40, type=int, help='max cut times')
    parser.add_argument('--min-cut', default=10, type=int, help='min cut times')
    parser.add_argument('output', help='output dir name')
    args = parser.parse_args()

    path_origin = os.path.join(args.output, 'origin')
    path_split = os.path.join(args.output, 'split')
    os.makedirs(path_origin, exist_ok=True)
    os.makedirs(path_split, exist_ok=True)

    original = []
    logging.info('Start generating chromosome')
    for i in range(args.number):
        length = int(np.random.randn() * args.std + args.length)
        c = Chromosome.make(length, args.atom)
        original.append(c)
        logging.info(f'Generate chromosome {i + 1:02}/{args.number:02}, length {length}')
        c.save(os.path.join(path_origin, f'{c.label}.txt'))

    split = []
    logging.info('Start splitting')
    for i in range(args.number):
        cut_times = np.random.randint(args.min_cut, args.max_cut)
        cs = split_random(original[i], cut_times)
        logging.info(f'Splitting {original[i].label}, {i:02} pieces')
        split.extend(cs)

    logging.info('Shuffling')
    np.random.shuffle(split)
    logging.info('Start writing splits')
    for i, c in enumerate(split):
        logging.info(f'Write split {i}/{len(split)} {c.label}')
        c.save(os.path.join(path_split, f'{i}.txt'))
