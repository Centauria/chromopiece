from chromo import Chromosome, split_random

if __name__ == '__main__':
    c = Chromosome.make(1000)
    print(c)
    cs = split_random(c)
    for ch in cs:
        print(ch)
