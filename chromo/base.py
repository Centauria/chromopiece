# -*- coding: utf-8 -*-
import base64
import hashlib
from typing import Optional, List

import numpy as np


def md5_chromosome(data):
    return base64.urlsafe_b64encode(hashlib.md5(bytes(data)).digest()).decode()


class Chromosome:
    bp_dict = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-_'

    def __init__(self, data: Optional[np.ndarray] = None, label: Optional[str] = None):
        self.data = data
        if data is not None:
            self.label = label or md5_chromosome(data)

    def __repr__(self):
        data = "".join(self.data.astype(np.str)) if self.data is not None else ""
        if len(data) > 30:
            data = f'{data[:15]} ... {data[-15:]}'
        return f'C[{self.label}]({data})'

    @staticmethod
    def make(length: int, atom: int = 4, label: Optional[str] = None):
        if atom > 64:
            raise ValueError('atom must be <= 64')
        c = Chromosome()
        c.data = np.array(list(map(lambda n: Chromosome.bp_dict[n], np.random.randint(0, atom, length))), dtype=np.str)
        c.label = label or md5_chromosome(c.data)
        return c

    def split(self, point: int):
        assert point < self.data.shape[0]
        self.data, separation = self.data[:point], self.data[point:]
        return Chromosome(separation, self.label)

    def save(self, file_name: str):
        with open(file_name, 'w') as f:
            f.write(repr(self))


def split_random(chromosome: Chromosome, parts: int = 10) -> List[Chromosome]:
    chs = []
    points: np.ndarray = np.random.choice(np.arange(chromosome.data.shape[0]), parts, replace=False)
    points = points[np.argsort(-points)]
    for p in points:
        chs.append(chromosome.split(p))
    chs.append(chromosome)
    return chs
