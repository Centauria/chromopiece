# -*- coding: utf-8 -*-
from .base import Chromosome, split_random
from .io import load, save
