# -*- coding: utf-8 -*-
import re

import numpy as np

from chromo import Chromosome


def load(file_name: str):
    with open(file_name) as f:
        s = f.read()
        m = re.match(r'^C\[([-=\w]*)\]\((\w*)\)$', s)
        if m:
            label, data = m.groups()
            data = np.array(list(data), dtype=np.str)
            result = Chromosome(data, label)
        else:
            raise ValueError(f'File {file_name} is not a valid Chromosome format')
    return result


def save(chromosome: Chromosome, file_name: str):
    chromosome.save(file_name)
